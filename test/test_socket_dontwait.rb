require 'test/unit'
require 'io/nonblock'
$-w = true

CALLED = {}
class IO
  %w(read read_nonblock readpartial).each do |m|
    eval "
      alias #{m}_orig #{m}
      undef_method :#{m}
      def #{m}(*args)
        CALLED[:#{m}] = args
        #{m}_orig(*args)
      end
    "
  end
end

ORIGINAL_SOCKET = ENV['ORIGINAL_SOCKET'].to_i != 0
require(ORIGINAL_SOCKET ? 'socket' : 'socket_dontwait')

class TestSocketDontwait < Test::Unit::TestCase
  BS = ENV['bs'] ? ENV['bs'].to_i : 16384
  COUNT = ENV['count'] ? ENV['count'].to_i : 100000

  def setup
    @rd, @wr = UNIXSocket.pair
  end

  def teardown
    CALLED.clear
    @rd.close unless @rd.closed?
    @wr.close unless @wr.closed?
  end

  def test_write_read_nonblock_str
    assert_equal 3, @wr.write_nonblock('abc')
    assert_equal 'abc', @rd.read_nonblock(3)
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_write_read_nonblock_integer
    assert_equal 3, @wr.write_nonblock(123)
    assert_equal '123', @rd.read_nonblock(3)
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_invalid_read_length
    assert_raise(ArgumentError) { @rd.read(-1) }
    assert_raise(ArgumentError) { @rd.read(-1, "") }
    assert_raise(ArgumentError) { @rd.read_nonblock(-1) }
    assert_raise(ArgumentError) { @rd.read_nonblock(-1, "") }
    assert_raise(ArgumentError) { @rd.readpartial(-1) }
    assert_raise(ArgumentError) { @rd.readpartial(-1, "") }
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_read
    buf = '.' * 4093
    t = Thread.new { @rd.read 4096 }
    assert_equal 4093, @wr.write(buf)
    t.join(0)
    assert t.alive?
    assert_equal 4, @wr.write('....')
    t.join
    assert_equal('.' * 4096, t.value)
    assert_equal('.', @rd.read(1))
    @wr.close
    assert_nil @rd.read(1)
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_read_empty
    @wr.close
    assert_equal '', @rd.read
    str = ''
    str_object_id = str.object_id
    assert_equal str_object_id, @rd.read(nil, str).object_id
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_big_write_read_full_even
    tmp = IO.read('/dev/urandom', 16427) * 32
    tmp << '.' until ((tmp.bytesize % 16384) == 0)
    return
    pid = fork do
      nr = @wr.write(tmp)
      exit!(nr == tmp.bytesize)
    end
    @wr.close
    assert_equal tmp, @rd.read
    _, status = Process.waitpid2(pid)
    assert status.success?
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_big_write_read_full_odd
    n = @wr.getsockopt(Socket::SOL_SOCKET, Socket::SO_SNDBUF).unpack('i')[0]
    tmp = IO.read('/dev/urandom', 16427) * 32
    tmp << 'aasdfzf' until ((tmp.bytesize % 16384) != 0)
    pid = fork do
      nr = @wr.write(tmp)
      exit!(nr == tmp.bytesize)
    end
    @wr.close
    assert_equal tmp, @rd.read
    _, status = Process.waitpid2(pid)
    assert status.success?
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_zero_reads
    assert_equal '', @rd.read(0)
    assert_equal '', @rd.readpartial(0)
    assert_equal '', @rd.read_nonblock(0)

    buf = 'hello'
    assert_equal buf.object_id, @rd.read(0, buf).object_id
    assert_equal '', buf
    buf = 'hello'
    assert_equal buf.object_id, @rd.readpartial(0, buf).object_id
    assert_equal '', buf
    buf = 'hello'
    assert_equal buf.object_id, @rd.read_nonblock(0, buf).object_id
    assert_equal '', buf
  end

  def test_readpartial_signal
    r, w = IO.pipe
    pid = fork do
      r.close
      trap(:USR1) { w.syswrite "USR1" }
      w.syswrite "READY"
      rv = @rd.readpartial(16384)
      exit!(rv == "SIGNAL SAFE" && (ORIGINAL_SOCKET || CALLED.empty?))
    end
    w.close
    assert_equal "READY", r.read(5)
    assert_nothing_raised do
      sleep 0.5 # ugh, still potentially racy :<
      Process.kill(:USR1, pid)
    end
    assert_equal 'USR1', r.readpartial(4)
    assert_equal 11, @wr.write("SIGNAL SAFE")
    _, status = Process.waitpid2(pid)
    assert status.success?
  end

  def test_read_signal
    r, w = IO.pipe
    pid = fork do
      r.close
      trap(:USR1) { w.syswrite "USR1" }
      w.syswrite "READY"
      rv = @rd.read(11)
      exit!(rv == "SIGNAL SAFE" && (ORIGINAL_SOCKET || CALLED.empty?))
    end
    w.close
    assert_equal "READY", r.read(5)
    assert_nothing_raised do
      sleep 0.5 # ugh, still potentially racy :<
      Process.kill(:USR1, pid)
    end
    assert_equal 'USR1', r.readpartial(4)
    assert_equal 11, @wr.write("SIGNAL SAFE")
    _, status = Process.waitpid2(pid)
    assert status.success?
  end

  def test_closed_read
    @rd.close
    assert_raises(IOError) { @rd.read }
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_closed_write
    @wr.close
    assert_raises(IOError) { @wr.write 'hello' }
    assert_raises(IOError) { @wr.write_nonblock 'hello' }
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_closed_writer_read
    @wr.close
    assert_raises(EOFError) { @rd.readpartial 5 }
    assert_raises(EOFError) { @rd.read_nonblock 5 }
    assert_nil @rd.read(5)
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_closed_writer_read_with_buffer
    @wr.close
    buf = 'hello'
    assert_raises(EOFError) { @rd.readpartial 5, buf }
    assert_equal '', buf

    buf = 'hello'
    assert_raises(EOFError) { @rd.read_nonblock 5, buf }
    assert_equal '', buf

    buf = 'hello'
    assert_nil @rd.read(5, buf)
    assert_equal '', buf
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_closed_reader_write
    @rd.close
    assert_raises(Errno::EPIPE) { @wr.write 'hello' }
    assert_raises(Errno::EPIPE) { @wr.write_nonblock 'hello' }
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_readpartial_lock
    s = ""
    t = Thread.new { @rd.readpartial(5, s) }
    Thread.pass until s.size == 5
    assert_raise(RuntimeError) { s.clear }
    @wr.write "foobarbaz"
    @wr.close
    assert_equal("fooba", t.value)
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_read_lock
    s = ""
    t = Thread.new { @rd.read(5, s) }
    Thread.pass until s.size == 5
    assert_raise(RuntimeError) { s.clear }
    @wr.write "foobarbaz"
    @wr.close
    assert_equal("fooba", t.value)
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_close_write
    @wr.write "foobarbaz"
    @wr.close_write
    assert_equal("foobarbaz", @rd.read)
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_read_on_nonblock_socket
    s = ''
    @rd.nonblock = true
    assert @rd.nonblock?
    t = Thread.new { @rd.read(6, s) }
    Thread.pass until s.size == 6
    @wr.write 'abcdef'
    t.join
    assert_equal 'abcdef', t.value
    assert ! @rd.nonblock? unless ORIGINAL_SOCKET
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_write_nonblock_raises_eagain
    buf = "HELLO" * 1000
    assert_raises(Errno::EAGAIN) { loop { @wr.write_nonblock buf } }
  end

  def test_read_nonblock_raises_eagain
    assert_raises(Errno::EAGAIN) { @rd.read_nonblock 1 }
    assert CALLED.empty? unless ORIGINAL_SOCKET
  end

  def test_set_sync
    assert @wr.sync
    assert_nothing_raised { @wr.sync = false }
    assert @wr.sync
  end unless ORIGINAL_SOCKET

  def read_loop(io)
    rbuf = ""
    bs = BS
    n = bs * COUNT
    while io.read(bs, rbuf)
      n -= rbuf.size
    end
    0 == n
  end

  def write_loop(io)
    buf = " " * BS
    n = 0
    COUNT.times do
      n += io.write buf
    end
    io.close
    n == COUNT * BS
  end

  def test_read_write_loop_fork
    pid = fork do
      @wr.close
      exit read_loop(@rd)
    end
    @rd.close
    rv = write_loop(@wr)
    assert rv
    _, status = Process.waitpid2(pid)
    assert status.success?
  end

  def test_read_shared
    a = "." * 0x1000
    b = a.dup
    @wr.syswrite "a"
    assert_equal "a", @rd.readpartial(0x1000, a)
    assert_equal "a", a
    assert_equal "." * 0x1000, b
  end

  def test_read_shared_2
    a = "." * 0x1000
    b = a.dup
    @wr.syswrite "a"
    assert_equal "a", @rd.readpartial(0x1000, b)
    assert_equal "a", b
    assert_equal "." * 0x1000, a
  end
end
