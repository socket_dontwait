require 'mkmf'

have_func('rb_thread_blocking_region') or abort "Ruby 1.9 required"
have_func('rb_thread_io_blocking_region')
create_makefile('socket_dontwait_ext')
