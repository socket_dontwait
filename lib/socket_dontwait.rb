# -*- encoding: binary -*-
require 'socket'
require 'socket_dontwait_ext'

# SocketDontwait is included in the BasicSocket class automatically,
# allowing derived classes such as TCPSocket and UNIXSocket to
# inherit SocketDontwait methods
class BasicSocket
  include SocketDontwait
end
